FROM openjdk:11.0.9-jre-slim
EXPOSE 8080
ADD target/${project.artifactId}-${project.version}.jar app.jar
ENV JAVA_OPTS="-XX:MaxRAMPercentage=40.0 -XshowSettings:vm -Xlog:gc*:stdout:tags,level,time"
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Dspring.profiles.active=container -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
