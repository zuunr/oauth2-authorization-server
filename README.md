# oauth2-authorization-server

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)

This module contains services for generating new jwt access tokens for dev/test purpose. The access tokens are signed with an asymmetric key pair, and the public key is exposed as an endpoint for use with a spring boot oauth2 resource server.

## Supported tags

* [`1.0.1-5644267`, (*5644267/Dockerfile*)](https://bitbucket.org/zuunr/oauth2-authorization-server/src/5644267/Dockerfile)

## Usage

The entrypoint for this image is located at port 8080, which will need to be exposed.

The image has three properties which can be configured to suit your needs.

* -e ZUUNR_SECURITY_OAUTH2_AUTHORIZATIONSERVER_EXTERNALACCESSIBLEHOST='http://localhost:8080'
* -e ZUUNR_SECURITY_OAUTH2_AUTHORIZATIONSERVER_CONTEXTCLAIMNAMESPACE='https://restbed.zuunr.com/context'
* -e ZUUNR_SECURITY_OAUTH2_AUTHORIZATIONSERVER_ROLESCLAIMNAMESPACE='https://restbed.zuunr.com/roles'

Start the image by executing the following command, replacing <tag> with the appropriate tag.

```shell
docker run -it --rm -p 9090:8080 zuunr/oauth2-authorization-server:<tag>
```

This will download the image from docker hub and start the container.

### Resource server

Configure your spring boot oauth2 resource server issuer-uri to the started docker container, e.g. issuer-uri: http://localhost:9090/ for the above example.
