/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.oauth2.authorization.server.service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.zuunr.json.JsonObject;
import com.zuunr.oauth2.authorization.server.api.JwtRequest;
import com.zuunr.oauth2.authorization.server.config.Config;
import com.zuunr.oauth2.authorization.server.service.util.RSAKeyProvider;

import reactor.core.publisher.Mono;

/**
 * <p>The JwtTokenService is responsible for creating
 * tokens based on the user's {@link JwtRequest}.</p>
 * 
 * <p>The token is signed with the RSAKey from the 
 * {@link RSAKeyProvider}, with the {@link JWSAlgorithm#RS256}.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class JwtTokenService {
    
    private RSAKeyProvider rsaKeyProvider;
    private Config config;
    
    @Autowired
    public JwtTokenService(
            RSAKeyProvider rsaKeyProvider,
            Config config) {
        this.rsaKeyProvider = rsaKeyProvider;
        this.config = config;
    }
    
    public Mono<String> createToken(JwtRequest jwtRequest) {
        Date expiration = Date.from(OffsetDateTime.now(ZoneOffset.UTC)
                .plusSeconds(jwtRequest.getTimeToLiveInSeconds())
                .toInstant());
        
        JWTClaimsSet claimSet = new JWTClaimsSet.Builder()
                .audience(config.getExternalAccessibleHost())
                .issuer(config.getExternalAccessibleHost() + "/")
                .subject(jwtRequest.getSubject())
                .expirationTime(expiration)
                .claim(config.getRolesClaimNamespace(), jwtRequest.getRoles())
                .claim(config.getContextClaimNamespace(), jwtRequest.getContext())
                .build();
        
        RSAKey jwk = rsaKeyProvider.getKey();
        
        SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(jwk.getKeyID()).build(), claimSet);

        try {
            signedJWT.sign(new RSASSASigner(jwk));
        } catch (JOSEException e) {
            throw new RuntimeException("Could not sign JWT", e);
        }
        
        return Mono.just(JsonObject.EMPTY.put("token", signedJWT.serialize()).asJson());
    }
}
