/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.oauth2.authorization.server.endpoint;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.zuunr.oauth2.authorization.server.api.JwtRequest;
import com.zuunr.oauth2.authorization.server.service.JwkSetService;
import com.zuunr.oauth2.authorization.server.service.JwtTokenService;
import com.zuunr.oauth2.authorization.server.service.OpenidConfigurationService;

import reactor.core.publisher.Mono;

/**
 * <p>The RestEndpoint publishes endpoints for the API.</p>
 * 
 * @author Mikael Ahlberg
 */
@RestController
public class RestEndpoint {

    private final Logger logger = LoggerFactory.getLogger(RestEndpoint.class);

    private JwkSetService jwkSetService;
    private JwtTokenService jwtTokenService;
    private OpenidConfigurationService openidConfigurationService;

    @Autowired
    public RestEndpoint(
            JwkSetService jwkSetService,
            JwtTokenService jwtTokenService,
            OpenidConfigurationService openidConfigurationService) {
        this.jwkSetService = jwkSetService;
        this.jwtTokenService = jwtTokenService;
        this.openidConfigurationService = openidConfigurationService;
    }

    @GetMapping(path = ".well-known/openid-configuration", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getOpenidConfiguration(ServerHttpRequest httpRequest) {
        return Mono.just(httpRequest)
                .doOnNext(o -> logger.info("GET {}", o.getURI()))
                .flatMap(o -> openidConfigurationService.getOpenidConfiguration())
                .map(ResponseEntity::ok)
                .doOnNext(o -> logger.info("GET {}, responseBody: {}", httpRequest.getURI(), o))
                .onErrorMap(this::errorMapper);
    }
    
    @GetMapping(path = ".well-known/jwks.json", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getJwks(ServerHttpRequest httpRequest) {
        return Mono.just(httpRequest)
                .doOnNext(o -> logger.info("GET {}", o.getURI()))
                .flatMap(o -> jwkSetService.getPublicJWKSet())
                .map(ResponseEntity::ok)
                .doOnNext(o -> logger.info("GET {}, responseBody: {}", httpRequest.getURI(), o))
                .onErrorMap(this::errorMapper);
    }
    
    @PostMapping(path = "oauth2/token", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> createToken(ServerHttpRequest httpRequest, @Validated @RequestBody JwtRequest jwtRequest) {
        return Mono.just(jwtRequest)
                .doOnNext(o -> logger.info("POST {}, requestBody: {}", httpRequest.getURI(), o))
                .flatMap(jwtTokenService::createToken)
                .map(ResponseEntity::ok)
                .doOnNext(o -> logger.info("POST {}, responseBody: {}", httpRequest.getURI(), o))
                .onErrorMap(this::errorMapper);
    }

    private Throwable errorMapper(Throwable originalCause) {
        String id = UUID.randomUUID().toString();

        return new RuntimeException("An unexpected error occurred, id: " + id, originalCause);
    }
}
