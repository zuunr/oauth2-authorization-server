/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.oauth2.authorization.server.service.util;

import org.springframework.stereotype.Component;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;

/**
 * <p>The RSAKeyProvider is responsible for
 * creating and holding the {@link RSAKey}.</p>
 * 
 * <p>The bit size is 2048, and the keyID is set
 * to 'minimal-ASA'.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class RSAKeyProvider {
    
    private RSAKey key;
    
    /**
     * <p>Returns the {@link RSAKey}.</p>
     * 
     * @return an {@link RSAKey}
     */
    public RSAKey getKey() {
        if (key == null) {
            key = initKey();
        }
        
        return key;
    }
    
    private RSAKey initKey() {
        try {
            return new RSAKeyGenerator(2048)
                    .keyID("minimal-ASA")
                    .keyUse(KeyUse.SIGNATURE)
                    .generate();
        } catch (JOSEException e) {
            throw new RuntimeException("Could not create RSAKey", e);
        }
    }
}
