/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.oauth2.authorization.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObject;
import com.zuunr.oauth2.authorization.server.config.Config;

import reactor.core.publisher.Mono;

/**
 * <p>The OpenidConfigurationService is responsible
 * for returning the dummy openid-configuration.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class OpenidConfigurationService {
    
    private Config config;
    
    @Autowired
    public OpenidConfigurationService(Config config) {
        this.config = config;
    }
    
    public Mono<String> getOpenidConfiguration() {
        JsonObject openidConfiguration = JsonObject.EMPTY
                .put("issuer", config.getExternalAccessibleHost() + "/")
                .put("jwks_uri", config.getExternalAccessibleHost() + "/.well-known/jwks.json");
        
        return Mono.just(openidConfiguration.asJson());
    }
}
