/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.oauth2.authorization.server.config;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

/**
 * <p>The Config contains properties required
 * by the application.</p>
 *
 * @author Mikael Ahlberg
 */
@ConfigurationProperties(prefix = "zuunr.security.oauth2.authorizationserver")
@Validated
public class Config {
    
    @NotEmpty(message = "You must set the external accessible host, (default http://localhost:8080)")
    private final String externalAccessibleHost;
    @NotEmpty(message = "You must set the context claim namespace, (default https://restbed.zuunr.com/context)")
    private final String contextClaimNamespace;
    @NotEmpty(message = "You must set the roles claim namespace, (default https://restbed.zuunr.com/roles)")
    private final String rolesClaimNamespace;
    
    @ConstructorBinding
    public Config(
            String externalAccessibleHost,
            String contextClaimNamespace,
            String rolesClaimNamespace) {
        this.externalAccessibleHost = externalAccessibleHost;
        this.contextClaimNamespace = contextClaimNamespace;
        this.rolesClaimNamespace = rolesClaimNamespace;
    }

    public String getExternalAccessibleHost() {
        return externalAccessibleHost;
    }

    public String getContextClaimNamespace() {
        return contextClaimNamespace;
    }

    public String getRolesClaimNamespace() {
        return rolesClaimNamespace;
    }
}
