/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.oauth2.authorization.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nimbusds.jose.jwk.JWKSet;
import com.zuunr.oauth2.authorization.server.service.util.RSAKeyProvider;

import reactor.core.publisher.Mono;

/**
 * <p>The JwkSetService is responsible for
 * holding the {@link JWKSet} associated with this
 * authorization server implementation.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class JwkSetService {
    
    private JWKSet jwkSet;
    
    private RSAKeyProvider rsaKeyProvider;
    
    @Autowired
    public JwkSetService(RSAKeyProvider rsaKeyProvider) {
        this.rsaKeyProvider = rsaKeyProvider;
    }
    
    /**
     * <p>Retrieves the public {@link JWKSet} as a json
     * string.</p>
     * 
     * @return a json string containing the public JWKs
     */
    public Mono<String> getPublicJWKSet() {
        if (jwkSet == null) {
            jwkSet = new JWKSet(rsaKeyProvider.getKey());
        }
        
        return Mono.just(jwkSet.toPublicJWKSet().toJSONObject().toJSONString());
    }
}
