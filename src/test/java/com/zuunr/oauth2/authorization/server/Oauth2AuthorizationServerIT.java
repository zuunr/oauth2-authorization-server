/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.oauth2.authorization.server;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.zuunr.oauth2.authorization.server.api.JwtRequest;

/**
 * <p>Integration test for creating jwt tokens.</p> 
 * 
 * <p>The Spring boot app is started at a random port.</p>
 * 
 * @author Mikael Ahlberg
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class Oauth2AuthorizationServerIT {

    private @Autowired WebTestClient client;

    @Test
    void givenRequestShouldCreateToken() {
        JwtRequest request = JwtRequest.builder()
                .subject("laura_andersson")
                .context("sweden.stockholm")
                .timeToLiveInSeconds(3600)
                .roles(Arrays.asList("ADMIN"))
                .build();

        client.post()
            .uri("oauth2/token")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(request)
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody().jsonPath("token").exists();
    }
    
    @Test
    void givenRequestShouldReturnOpenidConfiguration() {
        client.get()
            .uri(".well-known/openid-configuration")
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody()
                .jsonPath("issuer").exists()
                .jsonPath("jwks_uri").exists();
    }
    
    @Test
    void givenRequestShouldReturnJWKs() {
        client.get()
            .uri(".well-known/jwks.json")
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody().jsonPath("keys").exists();
    }
}
